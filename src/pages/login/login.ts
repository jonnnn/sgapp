import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	loginForm: FormGroup;
	loginError: string;

  constructor(
  	public navCtrl: NavController
  	, public navParams: NavParams
  	, public fb: FormBuilder
  ) {
		this.loginForm = fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
		});
  }



  ionViewDidLoad() {
  }

}
